#!/bin/bash

# Comando para crear usuarios
comando_dspace="/dspace/bin/dspace user --add --email mail1 -g nombre1 -s apellido1 --password pass1"

# Obtener el número de líneas del archivo
lineas=$(wc -l ./usuarios.txt | awk '{print $1}')

echo "Total de usuarios: $lineas"
log_file="usuarios_creados.log"
exec >"$log_file" 2>&1


# Iterar por cada línea del archivo
for ((i = 1; i <= $lineas; i++)); do
    # Extraer información de usuario para la línea actual
    linea=$(cat ./usuarios.txt | sed -n "$i{p;}" )

    # Separar los campos de la línea usando awk
    mail=$(awk -F'"' '{print $2}' <<< "$linea")
    nombre=$(awk -F'"' '{print $4}' <<< "$linea")
    apellido=$(awk -F'"' '{print $6}' <<< "$linea")
    pass=$(awk -F'"' '{print $8}' <<< "$linea")

    # Reemplazar valores en el comando
    comando_final=$(echo $comando_dspace | sed "s/mail1/$mail/g" | sed "s/nombre1/$nombre/g" | sed "s/apellido1/$apellido/g" | sed "s/pass1/$pass/g")

echo $comando_final

    # Ejecutar el comando con los valores reemplazados
    echo "Creando usuario: $mail"
    $comando_final

    # Comprobar si la creación del usuario fue exitosa
    if [ $? -eq 0 ]; then
        echo "Usuario $mail creado exitosamente."
    else
        echo "Error al crear usuario $mail."
    fi
done

exec >&1
