# DSpace Bulk User Create



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ricardoortiz.salud/dspace-bulk-user-create.git
git branch -M main
git push -uf origin main
```
# Modo de uso:

Completar la lista de usuarios a crear en usuarios.txt con el siguiente formato:

```
"correo@servidor.com" "Nombre" "Apellido" "contraseña"
```

Ejecutar el script:

```
./newusers.sh
```

# Cambiar constraseña de un usuario:

Eejecutar el siguiente comando para el usuario deseado:

```
./dspace user -M -m test@test.net -w
```

Ingresar la nueva contraseña y reingresarla cuando lo solicite.